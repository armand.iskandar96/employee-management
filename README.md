Cara menjalankan aplikasi, pastikan Anda sudah menginstall nodejs dan angular, bila tidak bisa ikuti langkah berikut:
- Install Nodejs minimal versi 16
- Install Angular: npm install -g @angular/cli
- Clone repo ini
- Jalankan command berikut untuk menginstall semua dependency yang dibutuhkan: npm i --legacy-peer-deps
- Untuk memulai aplikasi ini jalankan command: ng serve -o
- Setelah menjalankan command di atas, sebuah browser akan terbuka secara otomatis

note: untuk login bisa menggunakan credential berikut
{
    username: admin,
    password: admin
}