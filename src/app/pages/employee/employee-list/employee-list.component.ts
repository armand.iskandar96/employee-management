import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/model/employee/employee-model';
import { EmployeeService } from 'src/app/model/employee/employee.service';
import { DummyService } from 'src/app/services/dummy/dummy.service';
import { GeneralService } from 'src/app/services/general/general.service';

@Component({
    selector: 'app-employee-list',
    templateUrl: './employee-list.component.html',
    styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
    employees: Employee[] = [];
    p: number = 1;
    totalItems: number = 0;
    itemsPerPage: number = 10;
    searchParams: any = { username: '', firstName: '', lastName: '' };

    constructor(private employeeService: EmployeeService, private router: Router, public generalService: GeneralService) { }

    ngOnInit(): void {
        this.loadEmployees();
        this.restoreSearchParams();
    }

    loadEmployees(): void {
        this.employeeService.getEmployees().subscribe(employees => {
            this.employees = employees;
            this.totalItems = employees.length; // Set total items count
        });
    }

    search(): void {

        const params = {
            username: this.searchParams.username.trim().toLowerCase(),
            firstName: this.searchParams.firstName.trim().toLowerCase(),
            lastName: this.searchParams.lastName.trim().toLowerCase()
        };

        this.employeeService.searchEmployees(this.searchParams).subscribe(employees => {
            this.employees = employees;
            this.totalItems = employees.length;
            this.saveSearchParams();
        });

    }

    navigateToDetail(username: string): void {
        this.saveSearchParams();
        sessionStorage.setItem('searchParams', JSON.stringify(this.searchParams));
        this.router.navigate(['/employees', username]);
    }

    onPageChange(pageNumber: number): void {
        this.p = pageNumber;
    }

    deleteEmployee(username: string): void {
        this.employeeService.deleteEmployee(username);
        this.loadEmployees();
    }

    private saveSearchParams(): void {
        sessionStorage.setItem('searchParams', JSON.stringify(this.searchParams));
    }

    private restoreSearchParams(): void {
        const storedParams = sessionStorage.getItem('searchParams');
        if (storedParams) {
            this.searchParams = JSON.parse(storedParams);
        }
    }
}
