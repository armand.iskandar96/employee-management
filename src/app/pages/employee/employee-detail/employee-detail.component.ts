import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from 'src/app/model/employee/employee-model';
import { EmployeeService } from 'src/app/model/employee/employee.service';
import { GeneralService } from 'src/app/services/general/general.service';

@Component({
    selector: 'app-employee-detail',
    templateUrl: './employee-detail.component.html',
    styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
    employee?: Employee;
    searchParams: any;

    constructor(
        public route: ActivatedRoute,
        private router: Router,
        private employeeService: EmployeeService,
        public generalService: GeneralService
    ) {
        this.searchParams = JSON.parse(sessionStorage.getItem('searchParams') || '{}');
    }

    ngOnInit(): void {
        const username = this.route.snapshot.paramMap.get('username');
        if (username) {
            this.employeeService.getEmployee(username).subscribe(employee => {
                this.employee = employee;
            });
        }
    }

    goBack(): void {
        this.router.navigate(['/employees']);
    }

}
