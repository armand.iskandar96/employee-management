import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Employee } from 'src/app/model/employee/employee-model';
import { EmployeeService } from 'src/app/model/employee/employee.service';
import { GroupService } from 'src/app/model/group/group.service';

@Component({
    selector: 'app-employee-form',
    templateUrl: './employee-form.component.html',
    styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
    employeeForm: FormGroup;
    isEditMode: boolean = false;
    groups: string[] = [];

    constructor(
        private fb: FormBuilder,
        private employeeService: EmployeeService,
        private groupService: GroupService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.employeeForm = this.fb.group({
            username: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            birthDate: [null, [Validators.required, this.maxDateValidator()]],
            basicSalary: ['', Validators.required],
            status: ['', Validators.required],
            group: ['', Validators.required],
            description: ['', Validators.required],
        });
    }

    ngOnInit(): void {
        /* CEK EDIT MODE OR CREATE MODE */
        const username = this.route.snapshot.paramMap.get('username');
        if (username) {
            this.isEditMode = true;
            this.employeeService.getEmployee(username).subscribe(employee => {
                if (employee) {
                    // Convert birthDate to ISO string to set the input type date correctly
                    console.log(employee.birthDate);
                    console.log(new Date(employee.birthDate).toISOString().split('T')[0]);
                    const employeeData = {
                        ...employee,
                        birthDate: new Date(employee.birthDate).toISOString().split('T')[0]
                    };
                    this.employeeForm.patchValue(employeeData);
                }
            });
        }

        /* POPULATE GROUP DATA */
        this.groups = this.groupService.group;
    }

    maxDateValidator() {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const selectedDate = moment(control.value, 'YYYY-MM-DD', true);
            const currentDate = moment();

            if (!selectedDate.isValid()) {
                return { 'invalidDate': true };
            }

            if (selectedDate.isAfter(currentDate, 'day')) {
                return { 'maxDate': true };
            }

            return null;
        };
    }

    onSubmit(): void {
        if (this.employeeForm.valid) {
            const employee: Employee = this.employeeForm.value;
            if (this.isEditMode) {
                this.employeeService.updateEmployee(employee);
            } else {
                this.employeeService.addEmployee(employee);
            }
            this.router.navigate(['/']);
        }
    }
}
