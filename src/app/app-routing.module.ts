import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'employees',
        loadChildren: () => import('./pages/employee/employee.module').then(
            (m) => m.EmployeeModule
        ),
        canLoad: [AuthGuard]
    },
    { path: '**', redirectTo: 'employees', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
