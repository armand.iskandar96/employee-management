import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employee } from './employee-model';
import { Observable, catchError, of, retry, tap, throwError } from 'rxjs';
import { DummyService } from 'src/app/services/dummy/dummy.service';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    /* data ini digenerate dengan faker.js, print json lalu paste di sini */
    employees: Employee[] = [
        {
            "username": "Gust_Pagac",
            "firstName": "Brent",
            "lastName": "Legros",
            "email": "Dewitt.Smitham3@gmail.com",
            "birthDate": new Date("1991-06-16"),
            "basicSalary": 10098968,
            "status": "Inactive",
            "group": "Admin",
            "description": "Vox clementia eligendi non ustulo approbo."
        },
        {
            "username": "Lelia8",
            "firstName": "Deon",
            "lastName": "O'Kon",
            "email": "Madonna6@hotmail.com",
            "birthDate": new Date("1991-06-13"),
            "basicSalary": 9778747,
            "status": "Active",
            "group": "Guest",
            "description": "Excepturi quia ait."
        },
        {
            "username": "Andy25",
            "firstName": "Lindsay",
            "lastName": "Mann",
            "email": "Kali_Effertz31@yahoo.com",
            "birthDate": new Date("1995-02-02"),
            "basicSalary": 5906178,
            "status": "Inactive",
            "group": "Admin",
            "description": "Tui valde cum."
        },
        {
            "username": "Dustin_Weimann2",
            "firstName": "Name",
            "lastName": "Weber",
            "email": "Lorine.Dach@gmail.com",
            "birthDate": new Date("1995-10-14"),
            "basicSalary": 15779781,
            "status": "Inactive",
            "group": "Guest",
            "description": "Tamquamzzz cupio viriliter utrum dens."
        },
        {
            "username": "Antonette_Schiller93",
            "firstName": "Oswaldo",
            "lastName": "Kihn",
            "email": "Rosalyn18@yahoo.com",
            "birthDate": new Date("1994-01-11"),
            "basicSalary": 18973933,
            "status": "Inactive",
            "group": "User",
            "description": "Sapiente summisse creta distinctio iure angulus consectetur timidus vilitas vestrum."
        },
        {
            "username": "Veronica91",
            "firstName": "Mohammad",
            "lastName": "Cartwright",
            "email": "Jerald_Kunde@hotmail.com",
            "birthDate": new Date("1999-10-01"),
            "basicSalary": 16819595,
            "status": "Active",
            "group": "Admin",
            "description": "Capio coma quibusdam caelestis angulus adulatio cuppedia."
        },
        {
            "username": "Cheyanne92",
            "firstName": "Eunice",
            "lastName": "Keebler",
            "email": "Tyreek_Towne@yahoo.com",
            "birthDate": new Date("1995-01-07"),
            "basicSalary": 6046245,
            "status": "Active",
            "group": "Guest",
            "description": "Suggero vilis curia."
        },
        {
            "username": "Dawn.Gusikowski39",
            "firstName": "Danielle",
            "lastName": "Schuppe",
            "email": "Bella.Grady23@yahoo.com",
            "birthDate": new Date("1995-08-12"),
            "basicSalary": 9235941,
            "status": "Active",
            "group": "Guest",
            "description": "Consectetur adhuc defetiscor vociferor."
        },
        {
            "username": "Nels17",
            "firstName": "Marcus",
            "lastName": "Torphy",
            "email": "Pearline_Okuneva10@hotmail.com",
            "birthDate": new Date("1996-01-13"),
            "basicSalary": 8159314,
            "status": "Active",
            "group": "Admin",
            "description": "Aperiam aperte supra argumentum carpo caste temporibus aufero charisma adfero."
        },
        {
            "username": "Alec.Leannon64",
            "firstName": "Lucie",
            "lastName": "Tremblay",
            "email": "Brannon32@gmail.com",
            "birthDate": new Date("1991-06-30"),
            "basicSalary": 19733067,
            "status": "Inactive",
            "group": "User",
            "description": "Cui delinquo temeritas aggredior collum verumtamen debilito volo."
        },
        {
            "username": "Bertrand_Boyle",
            "firstName": "Carlotta",
            "lastName": "Okuneva",
            "email": "Lenny35@gmail.com",
            "birthDate": new Date("1999-05-04"),
            "basicSalary": 6134370,
            "status": "Inactive",
            "group": "Guest",
            "description": "Degenero articulus utroque speculum."
        },
        {
            "username": "Joe.Connelly",
            "firstName": "Baron",
            "lastName": "Kassulke",
            "email": "Fern.OKeefe85@hotmail.com",
            "birthDate": new Date("1994-08-29"),
            "basicSalary": 15611941,
            "status": "Active",
            "group": "Guest",
            "description": "Incidunt patruus ambitus colligo soluta."
        },
        {
            "username": "Edwardo60",
            "firstName": "Adrienne",
            "lastName": "Rice",
            "email": "Jarrod_Hane48@hotmail.com",
            "birthDate": new Date("1993-04-24"),
            "basicSalary": 15873943,
            "status": "Inactive",
            "group": "Admin",
            "description": "Vere aestivus verbum acies vulariter."
        },
        {
            "username": "Nola34",
            "firstName": "Samir",
            "lastName": "Lesch",
            "email": "Cordia_Hane93@yahoo.com",
            "birthDate": new Date("1992-08-24"),
            "basicSalary": 14786759,
            "status": "Active",
            "group": "Admin",
            "description": "Consectetur autem vulnero deficio vito sophismata."
        },
        {
            "username": "Francesco_Friesen0",
            "firstName": "Keyon",
            "lastName": "Lesch",
            "email": "Lonnie_Kuhn@hotmail.com",
            "birthDate": new Date("1997-05-02"),
            "basicSalary": 18129661,
            "status": "Active",
            "group": "Admin",
            "description": "Causa spectaculum mollitia angulus velum tum antepono capio concido tepesco."
        },
        {
            "username": "Isidro85",
            "firstName": "Marisol",
            "lastName": "MacGyver",
            "email": "Johnny_Hudson96@yahoo.com",
            "birthDate": new Date("1996-06-28"),
            "basicSalary": 9332304,
            "status": "Inactive",
            "group": "Admin",
            "description": "Bonus blanditiis veritatis coruscus dapifer verto vicinus aliquid."
        },
        {
            "username": "Jovany_Sauer72",
            "firstName": "Jamil",
            "lastName": "Wolf",
            "email": "Briana_Leffler@yahoo.com",
            "birthDate": new Date("2000-12-12"),
            "basicSalary": 18781028,
            "status": "Inactive",
            "group": "User",
            "description": "Teneo adversus ante aperio cribro verto terreo."
        },
        {
            "username": "Jaquan48",
            "firstName": "Eduardo",
            "lastName": "Von",
            "email": "Edwin.Runte@hotmail.com",
            "birthDate": new Date("1993-01-27"),
            "basicSalary": 8111466,
            "status": "Active",
            "group": "Guest",
            "description": "Strues minima clam modi bellicus terminatio admoveo admiratio uter dolorem."
        },
        {
            "username": "Julianne5",
            "firstName": "Jerod",
            "lastName": "Wyman",
            "email": "Esta.Klocko@yahoo.com",
            "birthDate": new Date("1996-10-11"),
            "basicSalary": 5974478,
            "status": "Active",
            "group": "Guest",
            "description": "Degenero apud subiungo adversus defendo."
        },
        {
            "username": "Junior66",
            "firstName": "Jeramy",
            "lastName": "Lubowitz-Halvorson",
            "email": "Dorian.Thompson31@gmail.com",
            "birthDate": new Date("1998-11-21"),
            "basicSalary": 12242078,
            "status": "Inactive",
            "group": "User",
            "description": "Adflicto viscus tenus absque adfectus."
        },
        {
            "username": "Dax0",
            "firstName": "Myrtis",
            "lastName": "Macejkovic",
            "email": "Raphaelle_Bayer12@gmail.com",
            "birthDate": new Date("1995-12-08"),
            "basicSalary": 15629557,
            "status": "Active",
            "group": "Admin",
            "description": "Auctor textor tergum vitae."
        },
        {
            "username": "Destiney82",
            "firstName": "Joaquin",
            "lastName": "Gerlach",
            "email": "Clifton_Rodriguez@gmail.com",
            "birthDate": new Date("1991-01-16"),
            "basicSalary": 12626711,
            "status": "Inactive",
            "group": "User",
            "description": "Trucido damno considero spes tenetur."
        },
        {
            "username": "Keagan.Cole",
            "firstName": "Federico",
            "lastName": "Rohan",
            "email": "Laurine85@gmail.com",
            "birthDate": new Date("2000-09-08"),
            "basicSalary": 7983669,
            "status": "Active",
            "group": "Admin",
            "description": "Catena tutis vel."
        },
        {
            "username": "Pietro_Denesik",
            "firstName": "Nedra",
            "lastName": "Sanford",
            "email": "Jorge90@hotmail.com",
            "birthDate": new Date("1994-03-07"),
            "basicSalary": 17807989,
            "status": "Inactive",
            "group": "User",
            "description": "Suspendo stips urbanus confugo aliquid."
        },
        {
            "username": "Sydnee10",
            "firstName": "Brock",
            "lastName": "McCullough",
            "email": "Patrick_Hand60@hotmail.com",
            "birthDate": new Date("1994-08-04"),
            "basicSalary": 12452878,
            "status": "Inactive",
            "group": "Admin",
            "description": "Ait aeternus crebro uter unde."
        },
        {
            "username": "Golden76",
            "firstName": "Lorena",
            "lastName": "Kshlerin",
            "email": "Elody_Miller58@yahoo.com",
            "birthDate": new Date("1998-04-18"),
            "basicSalary": 14537921,
            "status": "Inactive",
            "group": "Guest",
            "description": "Stips suasoria tempora curia."
        },
        {
            "username": "Keely.Jast72",
            "firstName": "Kadin",
            "lastName": "Boyle",
            "email": "Karianne_Heathcote96@yahoo.com",
            "birthDate": new Date("1994-04-19"),
            "basicSalary": 6093411,
            "status": "Active",
            "group": "User",
            "description": "Demoror suppono truculenter cinis suspendo conventus."
        },
        {
            "username": "Irving.Willms21",
            "firstName": "Joanie",
            "lastName": "Jast",
            "email": "Samson23@yahoo.com",
            "birthDate": new Date("1995-12-03"),
            "basicSalary": 12399161,
            "status": "Active",
            "group": "User",
            "description": "Demoror vomer venio termes modi studio quibusdam comparo."
        },
        {
            "username": "Molly.Kassulke50",
            "firstName": "Olin",
            "lastName": "Abbott",
            "email": "Kiera.Schowalter-Klein@hotmail.com",
            "birthDate": new Date("2000-10-22"),
            "basicSalary": 12711200,
            "status": "Active",
            "group": "Admin",
            "description": "Tempus bestia conservo vespillo atqui aranea."
        },
        {
            "username": "Cassandra57",
            "firstName": "Alicia",
            "lastName": "Lynch",
            "email": "Major_Dietrich49@yahoo.com",
            "birthDate": new Date("1991-10-18"),
            "basicSalary": 17660687,
            "status": "Active",
            "group": "User",
            "description": "Compono decor ocer vicinus."
        },
        {
            "username": "Rod_Fritsch70",
            "firstName": "Hadley",
            "lastName": "Stark",
            "email": "Rozella_Ruecker51@gmail.com",
            "birthDate": new Date("1998-10-15"),
            "basicSalary": 11129219,
            "status": "Inactive",
            "group": "User",
            "description": "Tredecim spiculum repellat vallum vel sodalitas eum cena."
        },
        {
            "username": "Earnest42",
            "firstName": "Megane",
            "lastName": "Legros",
            "email": "Oma_Gutmann@yahoo.com",
            "birthDate": new Date("1999-01-15"),
            "basicSalary": 19304752,
            "status": "Inactive",
            "group": "Guest",
            "description": "Commodi defaeco admiratio delibero utroque depraedor."
        },
        {
            "username": "Jerel.Dicki49",
            "firstName": "Trinity",
            "lastName": "Kuhic",
            "email": "Linwood.Cassin-Hirthe64@gmail.com",
            "birthDate": new Date("2000-05-13"),
            "basicSalary": 17772021,
            "status": "Active",
            "group": "Guest",
            "description": "Audax speciosus cupiditate aetas."
        },
        {
            "username": "Manley99",
            "firstName": "Cristal",
            "lastName": "Baumbach",
            "email": "Avery_Rutherford@gmail.com",
            "birthDate": new Date("1997-03-12"),
            "basicSalary": 12581513,
            "status": "Active",
            "group": "User",
            "description": "Crustulum curto vociferor ratione."
        },
        {
            "username": "Rowena_Ferry7",
            "firstName": "Janice",
            "lastName": "Stracke",
            "email": "Merl.Shields@yahoo.com",
            "birthDate": new Date("1999-11-28"),
            "basicSalary": 5546216,
            "status": "Active",
            "group": "Admin",
            "description": "Carpo totus tersus ustilo nostrum veritas templum."
        },
        {
            "username": "Melyssa.Carroll41",
            "firstName": "Kirk",
            "lastName": "Ward",
            "email": "Era67@yahoo.com",
            "birthDate": new Date("1995-12-11"),
            "basicSalary": 9001841,
            "status": "Inactive",
            "group": "User",
            "description": "Spoliatio sulum apto annus spiritus celer."
        },
        {
            "username": "Natalia.Macejkovic63",
            "firstName": "Melany",
            "lastName": "Watsica",
            "email": "Ella_Jakubowski@gmail.com",
            "birthDate": new Date("1991-03-19"),
            "basicSalary": 18958790,
            "status": "Inactive",
            "group": "Guest",
            "description": "Vicinus bis contigo tondeo iusto vinum ademptio aegrotatio perferendis sto."
        },
        {
            "username": "Abbey41",
            "firstName": "Javon",
            "lastName": "Funk",
            "email": "Lennie.Mayert@yahoo.com",
            "birthDate": new Date("2000-10-07"),
            "basicSalary": 14407140,
            "status": "Inactive",
            "group": "Guest",
            "description": "Tantum tamdiu totam vobis curtus torqueo fugit delibero vicissitudo caecus."
        },
        {
            "username": "Sharon_MacGyver",
            "firstName": "Marianne",
            "lastName": "Mayer",
            "email": "Johnpaul_OKeefe@yahoo.com",
            "birthDate": new Date("1996-10-14"),
            "basicSalary": 11333776,
            "status": "Active",
            "group": "Admin",
            "description": "Maxime vulgivagus vapulus tergiversatio chirographum color peior ab."
        },
        {
            "username": "Juana.Mills",
            "firstName": "Avis",
            "lastName": "Kilback",
            "email": "Sadie_Wilderman@gmail.com",
            "birthDate": new Date("2000-08-07"),
            "basicSalary": 8650583,
            "status": "Inactive",
            "group": "User",
            "description": "Valetudo thesaurus contego benigne."
        },
        {
            "username": "Madelyn.Huel99",
            "firstName": "Curt",
            "lastName": "Aufderhar",
            "email": "Chauncey_Krajcik@gmail.com",
            "birthDate": new Date("1996-12-01"),
            "basicSalary": 14148866,
            "status": "Inactive",
            "group": "Guest",
            "description": "Pax aurum demonstro stips tantillus asporto."
        },
        {
            "username": "Aniya.Ortiz",
            "firstName": "Garfield",
            "lastName": "Mitchell",
            "email": "Kari_Yost@yahoo.com",
            "birthDate": new Date("1998-05-04"),
            "basicSalary": 12353211,
            "status": "Inactive",
            "group": "Admin",
            "description": "Officiis magnam rem vespillo tracto sumptus."
        },
        {
            "username": "Lew_Price",
            "firstName": "Freeman",
            "lastName": "Grant",
            "email": "Olaf_Blanda57@gmail.com",
            "birthDate": new Date("2000-05-26"),
            "basicSalary": 12648892,
            "status": "Inactive",
            "group": "Guest",
            "description": "Auctus custodia crapula tamdiu undique."
        },
        {
            "username": "Kassandra_Schimmel74",
            "firstName": "Mallory",
            "lastName": "O'Conner",
            "email": "Jessika.Reilly30@gmail.com",
            "birthDate": new Date("1994-12-09"),
            "basicSalary": 17900921,
            "status": "Inactive",
            "group": "Admin",
            "description": "Aranea unde magni damnatio."
        },
        {
            "username": "Martin.Keeling22",
            "firstName": "Alexandrea",
            "lastName": "Terry",
            "email": "Anita.Sipes@hotmail.com",
            "birthDate": new Date("1997-04-12"),
            "basicSalary": 11392223,
            "status": "Active",
            "group": "Admin",
            "description": "Degusto denuo circumvenio umbra deleo thymbra ultra dignissimos."
        },
        {
            "username": "Art.Lehner",
            "firstName": "Tamara",
            "lastName": "Lehner",
            "email": "Nolan_Lehner23@yahoo.com",
            "birthDate": new Date("1991-09-30"),
            "basicSalary": 12568300,
            "status": "Inactive",
            "group": "Admin",
            "description": "Vulpes arbor deorsum certus."
        },
        {
            "username": "Enoch.Ebert32",
            "firstName": "Antonette",
            "lastName": "Towne",
            "email": "Hermina_Muller@gmail.com",
            "birthDate": new Date("1996-06-22"),
            "basicSalary": 6699256,
            "status": "Inactive",
            "group": "Admin",
            "description": "Denego sol et molestiae claudeo tubineus cohors."
        },
        {
            "username": "Chaim.Morissette-Ward",
            "firstName": "Jarret",
            "lastName": "Yost",
            "email": "Tatyana.Wehner60@hotmail.com",
            "birthDate": new Date("2000-12-04"),
            "basicSalary": 9224094,
            "status": "Active",
            "group": "User",
            "description": "Ocer valetudo textus maxime."
        },
        {
            "username": "Damian50",
            "firstName": "Miller",
            "lastName": "Jacobi",
            "email": "Vicenta49@gmail.com",
            "birthDate": new Date("1997-08-16"),
            "basicSalary": 17048516,
            "status": "Active",
            "group": "Admin",
            "description": "Solus suggero carbo confero antepono sub beatus."
        },
        {
            "username": "Krystina_Hoppe36",
            "firstName": "Jayne",
            "lastName": "Reinger-Gerlach",
            "email": "Pietro_Kessler63@gmail.com",
            "birthDate": new Date("1998-05-20"),
            "basicSalary": 5865012,
            "status": "Inactive",
            "group": "Guest",
            "description": "Atrocitas surculus utrum nemo temperantia arcesso trucido."
        },
        {
            "username": "Caleb76",
            "firstName": "Kirk",
            "lastName": "Aufderhar",
            "email": "Suzanne.Morissette-Casper70@yahoo.com",
            "birthDate": new Date("1998-09-07"),
            "basicSalary": 14350146,
            "status": "Inactive",
            "group": "Guest",
            "description": "Cerno territo adimpleo cur desipio."
        },
        {
            "username": "Lue7",
            "firstName": "Kasandra",
            "lastName": "Johnston",
            "email": "Mortimer52@hotmail.com",
            "birthDate": new Date("1998-12-18"),
            "basicSalary": 5438101,
            "status": "Active",
            "group": "Guest",
            "description": "Aestivus aureus decipio auxilium."
        },
        {
            "username": "Kaelyn.Labadie",
            "firstName": "Julie",
            "lastName": "Homenick",
            "email": "Marilyne38@gmail.com",
            "birthDate": new Date("1997-01-23"),
            "basicSalary": 12036540,
            "status": "Inactive",
            "group": "Admin",
            "description": "Asporto timor voluptates timidus bibo defetiscor crux quas sustineo vomer."
        },
        {
            "username": "Jean_Feil",
            "firstName": "Joseph",
            "lastName": "Rath-D'Amore",
            "email": "Casper_Rice@yahoo.com",
            "birthDate": new Date("1996-09-11"),
            "basicSalary": 9300938,
            "status": "Inactive",
            "group": "User",
            "description": "Timor tonsor tres repellat custodia voluptatem solus vorago."
        },
        {
            "username": "Willow.Zboncak84",
            "firstName": "Cassandra",
            "lastName": "Mitchell",
            "email": "Jaunita_Williamson87@yahoo.com",
            "birthDate": new Date("1999-02-15"),
            "basicSalary": 17486374,
            "status": "Active",
            "group": "User",
            "description": "Maiores cupiditate peior porro dolores apto volaticus condico dapifer."
        },
        {
            "username": "Joelle46",
            "firstName": "Gerald",
            "lastName": "Satterfield",
            "email": "Jalen94@hotmail.com",
            "birthDate": new Date("2000-11-17"),
            "basicSalary": 16827315,
            "status": "Active",
            "group": "Admin",
            "description": "A tergiversatio voluntarius."
        },
        {
            "username": "Ottilie64",
            "firstName": "Maya",
            "lastName": "Kutch",
            "email": "Brayan24@gmail.com",
            "birthDate": new Date("1997-08-13"),
            "basicSalary": 9535822,
            "status": "Inactive",
            "group": "Guest",
            "description": "Pecto stultus validus degenero derelinquo."
        },
        {
            "username": "Allie49",
            "firstName": "Hillary",
            "lastName": "Gutkowski",
            "email": "Serena_Dickinson53@hotmail.com",
            "birthDate": new Date("1999-11-28"),
            "basicSalary": 6011634,
            "status": "Active",
            "group": "Admin",
            "description": "Trepide amor testimonium alo tabgo auditor apparatus vitae."
        },
        {
            "username": "Sierra_Buckridge",
            "firstName": "Brett",
            "lastName": "Rodriguez",
            "email": "Wilhelm_Welch12@yahoo.com",
            "birthDate": new Date("1991-12-04"),
            "basicSalary": 13959053,
            "status": "Active",
            "group": "Admin",
            "description": "Versus laborum cattus suspendo sunt cotidie adficio sui spargo crudelis."
        },
        {
            "username": "Hannah88",
            "firstName": "Maximilian",
            "lastName": "Renner",
            "email": "Emmet_Runolfsdottir@hotmail.com",
            "birthDate": new Date("1997-07-26"),
            "basicSalary": 19107492,
            "status": "Active",
            "group": "Admin",
            "description": "Vapulus neque sit verus auxilium."
        },
        {
            "username": "Marisa.Lubowitz51",
            "firstName": "Joseph",
            "lastName": "McGlynn",
            "email": "Sean.Hartmann@gmail.com",
            "birthDate": new Date("1995-07-25"),
            "basicSalary": 12196297,
            "status": "Active",
            "group": "Guest",
            "description": "Tandem conspergo bestia."
        },
        {
            "username": "Rudolph3",
            "firstName": "Marcelino",
            "lastName": "Schowalter",
            "email": "Aditya.Corwin@hotmail.com",
            "birthDate": new Date("1998-10-20"),
            "basicSalary": 13394863,
            "status": "Active",
            "group": "Guest",
            "description": "Textus sapiente adflicto amaritudo conicio."
        },
        {
            "username": "Adell_Gulgowski-Hudson",
            "firstName": "Bonita",
            "lastName": "Schultz",
            "email": "Fermin_Kilback96@yahoo.com",
            "birthDate": new Date("1991-05-25"),
            "basicSalary": 10669776,
            "status": "Inactive",
            "group": "User",
            "description": "Abbas sumo eum vulgus theologus veritas cresco carpo."
        },
        {
            "username": "Trace_Terry",
            "firstName": "Lennie",
            "lastName": "Von",
            "email": "Kathryne_Beier@gmail.com",
            "birthDate": new Date("2000-01-01"),
            "basicSalary": 6093883,
            "status": "Active",
            "group": "Admin",
            "description": "Quidem adiuvo spoliatio addo amo addo catena ustilo absque adnuo."
        },
        {
            "username": "Heath_Hackett83",
            "firstName": "Zackery",
            "lastName": "Stiedemann",
            "email": "Aurore3@yahoo.com",
            "birthDate": new Date("1994-03-28"),
            "basicSalary": 19738571,
            "status": "Active",
            "group": "Guest",
            "description": "Combibo casso coniuratio absum magni."
        },
        {
            "username": "Camylle_Fay49",
            "firstName": "Americo",
            "lastName": "Schuster",
            "email": "Fausto_Bergstrom@hotmail.com",
            "birthDate": new Date("1996-01-22"),
            "basicSalary": 6420757,
            "status": "Inactive",
            "group": "Guest",
            "description": "Quasi curatio territo tenuis derelinquo cibo nesciunt arcesso."
        },
        {
            "username": "Prudence.Bogan43",
            "firstName": "Annabell",
            "lastName": "Schiller",
            "email": "Roger.Shields95@yahoo.com",
            "birthDate": new Date("1997-07-08"),
            "basicSalary": 6373256,
            "status": "Inactive",
            "group": "User",
            "description": "Conventus nesciunt bene absconditus."
        },
        {
            "username": "Marcelina83",
            "firstName": "Junius",
            "lastName": "Sauer",
            "email": "Raina.Littel@yahoo.com",
            "birthDate": new Date("1993-08-25"),
            "basicSalary": 9656074,
            "status": "Active",
            "group": "User",
            "description": "Temptatio cruciamentum denuncio acceptus sollers allatus."
        },
        {
            "username": "Michelle70",
            "firstName": "Dedrick",
            "lastName": "Wisozk",
            "email": "Jasen73@yahoo.com",
            "birthDate": new Date("1993-09-02"),
            "basicSalary": 5603336,
            "status": "Inactive",
            "group": "Guest",
            "description": "Carbo curtus color cedo vinitor."
        },
        {
            "username": "Kallie.Reichel94",
            "firstName": "Nedra",
            "lastName": "Russel",
            "email": "Dorian.McClure12@gmail.com",
            "birthDate": new Date("1999-10-08"),
            "basicSalary": 10422250,
            "status": "Active",
            "group": "Guest",
            "description": "Talus stips odit utpote tripudio crur."
        },
        {
            "username": "Serenity_Littel83",
            "firstName": "Braxton",
            "lastName": "Boehm",
            "email": "Aurelie26@hotmail.com",
            "birthDate": new Date("1994-03-31"),
            "basicSalary": 14703357,
            "status": "Inactive",
            "group": "Admin",
            "description": "Defessus vinitor supra stillicidium textus."
        },
        {
            "username": "Donny76",
            "firstName": "Coralie",
            "lastName": "Kuhn",
            "email": "Jenifer.Larson@yahoo.com",
            "birthDate": new Date("1999-11-03"),
            "basicSalary": 10237855,
            "status": "Inactive",
            "group": "Admin",
            "description": "Laborum bonus dapifer."
        },
        {
            "username": "Vincenza_Bergstrom",
            "firstName": "Assunta",
            "lastName": "Schumm",
            "email": "Everette.Cruickshank@hotmail.com",
            "birthDate": new Date("1997-03-27"),
            "basicSalary": 19180910,
            "status": "Inactive",
            "group": "Admin",
            "description": "Varius adfero tersus absque defessus tribuo via tabesco."
        },
        {
            "username": "Lydia10",
            "firstName": "Jaron",
            "lastName": "Heller",
            "email": "Caesar_Wiza64@gmail.com",
            "birthDate": new Date("1993-04-24"),
            "basicSalary": 10508300,
            "status": "Active",
            "group": "Admin",
            "description": "Damnatio vestrum cursim molestiae esse."
        },
        {
            "username": "America.Ruecker",
            "firstName": "Linwood",
            "lastName": "Gerhold",
            "email": "Jaylin_Mueller@yahoo.com",
            "birthDate": new Date("1991-06-21"),
            "basicSalary": 5790429,
            "status": "Inactive",
            "group": "Admin",
            "description": "Vulgo tristis angulus tempora."
        },
        {
            "username": "Summer43",
            "firstName": "Kathryne",
            "lastName": "Glover",
            "email": "Damian30@gmail.com",
            "birthDate": new Date("1999-09-01"),
            "basicSalary": 13187751,
            "status": "Inactive",
            "group": "User",
            "description": "Clibanus velociter vesica defero solum."
        },
        {
            "username": "Xavier.Fahey",
            "firstName": "Jasper",
            "lastName": "Pagac",
            "email": "Agustina_Mante@yahoo.com",
            "birthDate": new Date("1991-03-01"),
            "basicSalary": 17389098,
            "status": "Inactive",
            "group": "User",
            "description": "Paens quis alii vulnero sonitus benigne versus ago tempus dolorem."
        },
        {
            "username": "Eldred.Kuvalis61",
            "firstName": "Paolo",
            "lastName": "Blanda",
            "email": "Barry93@gmail.com",
            "birthDate": new Date("1991-02-14"),
            "basicSalary": 18433530,
            "status": "Active",
            "group": "User",
            "description": "Averto rerum tenus bardus ago dolores vesica deprecator est."
        },
        {
            "username": "Brittany_Pollich78",
            "firstName": "Anastasia",
            "lastName": "Baumbach",
            "email": "Ava_Lang89@hotmail.com",
            "birthDate": new Date("1991-12-01"),
            "basicSalary": 16174191,
            "status": "Inactive",
            "group": "User",
            "description": "Curso sufficio subnecto terror carbo aufero sollicito."
        },
        {
            "username": "Trinity.Watsica",
            "firstName": "Antonia",
            "lastName": "Becker",
            "email": "Dorian83@yahoo.com",
            "birthDate": new Date("1994-10-23"),
            "basicSalary": 5549906,
            "status": "Active",
            "group": "User",
            "description": "Conduco cicuta apto crapula."
        },
        {
            "username": "Emmy.Donnelly",
            "firstName": "Jailyn",
            "lastName": "Johnson",
            "email": "Ephraim.Waters@yahoo.com",
            "birthDate": new Date("1997-03-04"),
            "basicSalary": 18653185,
            "status": "Inactive",
            "group": "Admin",
            "description": "Callide adeo deorsum vomer cohors deprimo vitium desipio adiuvo basium."
        },
        {
            "username": "Jaquelin_Barrows",
            "firstName": "Trenton",
            "lastName": "Dietrich",
            "email": "Mustafa62@hotmail.com",
            "birthDate": new Date("1999-10-23"),
            "basicSalary": 19034972,
            "status": "Active",
            "group": "Admin",
            "description": "Iste urbanus clam tergo surculus aestas."
        },
        {
            "username": "Ray.Schuppe22",
            "firstName": "Kenya",
            "lastName": "Beahan",
            "email": "Vivien1@gmail.com",
            "birthDate": new Date("1999-02-27"),
            "basicSalary": 12054532,
            "status": "Inactive",
            "group": "User",
            "description": "Cena verbera adeo corona."
        },
        {
            "username": "Lesley93",
            "firstName": "Grayson",
            "lastName": "Nitzsche",
            "email": "Priscilla.Jacobs@yahoo.com",
            "birthDate": new Date("1995-01-26"),
            "basicSalary": 11987223,
            "status": "Inactive",
            "group": "Guest",
            "description": "Vomica decens voro pectus coepi textilis sollicito defetiscor nobis ducimus."
        },
        {
            "username": "Jabari23",
            "firstName": "Ronaldo",
            "lastName": "Collier",
            "email": "Tracey.McLaughlin94@yahoo.com",
            "birthDate": new Date("1993-03-27"),
            "basicSalary": 17273190,
            "status": "Active",
            "group": "User",
            "description": "Vociferor vomer culpo studio suggero."
        },
        {
            "username": "Zoie_Ruecker4",
            "firstName": "Devon",
            "lastName": "Ritchie",
            "email": "Carolina30@hotmail.com",
            "birthDate": new Date("1995-01-01"),
            "basicSalary": 10655401,
            "status": "Active",
            "group": "Guest",
            "description": "Casus textus correptius caries contigo quos officia turpis."
        },
        {
            "username": "Ophelia87",
            "firstName": "Bernie",
            "lastName": "Pfeffer",
            "email": "Sandrine.Keeling@hotmail.com",
            "birthDate": new Date("1992-04-08"),
            "basicSalary": 7482137,
            "status": "Inactive",
            "group": "Admin",
            "description": "Complectus blanditiis statua uterque cognatus sustineo valetudo."
        },
        {
            "username": "Devyn72",
            "firstName": "Emory",
            "lastName": "Greenfelder",
            "email": "Ryder2@yahoo.com",
            "birthDate": new Date("1991-09-21"),
            "basicSalary": 8211624,
            "status": "Inactive",
            "group": "Guest",
            "description": "Solitudo tolero ipsam clam aegrus velut aggredior vesica."
        },
        {
            "username": "Wilma_Sipes",
            "firstName": "Deondre",
            "lastName": "Bayer",
            "email": "Kellie.Shields@yahoo.com",
            "birthDate": new Date("1992-11-18"),
            "basicSalary": 19323299,
            "status": "Inactive",
            "group": "User",
            "description": "Crustulum arceo vesica."
        },
        {
            "username": "Tatyana_Zieme23",
            "firstName": "Ima",
            "lastName": "Daugherty",
            "email": "Berry87@yahoo.com",
            "birthDate": new Date("1998-04-20"),
            "basicSalary": 11881363,
            "status": "Inactive",
            "group": "User",
            "description": "Saepe amplitudo admitto vacuus aegre vulnero undique quo compello."
        },
        {
            "username": "Estella.Weissnat",
            "firstName": "Kavon",
            "lastName": "Kihn",
            "email": "Abner.Shields35@hotmail.com",
            "birthDate": new Date("1991-05-10"),
            "basicSalary": 11076305,
            "status": "Active",
            "group": "Admin",
            "description": "Modi aiunt solitudo virgo explicabo repudiandae asper culpo accusamus demo."
        },
        {
            "username": "Carlo.Olson",
            "firstName": "Alysha",
            "lastName": "Homenick",
            "email": "Charity_Ratke27@yahoo.com",
            "birthDate": new Date("1993-04-15"),
            "basicSalary": 19364262,
            "status": "Active",
            "group": "Guest",
            "description": "Crustulum amitto virga eligendi abundans."
        },
        {
            "username": "Arnoldo.Fay",
            "firstName": "Patrick",
            "lastName": "Runolfsson",
            "email": "Vicente.Rath@yahoo.com",
            "birthDate": new Date("2000-01-19"),
            "basicSalary": 9130181,
            "status": "Active",
            "group": "User",
            "description": "Tum audax ambitus error decerno audacia."
        },
        {
            "username": "Blanche_DAmore68",
            "firstName": "Vincenzo",
            "lastName": "Hilpert",
            "email": "Ashton.King12@yahoo.com",
            "birthDate": new Date("1999-01-09"),
            "basicSalary": 16839281,
            "status": "Inactive",
            "group": "Admin",
            "description": "Supra tergiversatio tondeo carmen demergo tunc."
        },
        {
            "username": "Jacklyn52",
            "firstName": "Martine",
            "lastName": "Christiansen",
            "email": "Dee_Beatty54@yahoo.com",
            "birthDate": new Date("1993-08-24"),
            "basicSalary": 9517600,
            "status": "Inactive",
            "group": "Admin",
            "description": "Averto acerbitas subvenio."
        },
        {
            "username": "Aditya.Jaskolski",
            "firstName": "Blaze",
            "lastName": "Murphy",
            "email": "Charity_Sipes@gmail.com",
            "birthDate": new Date("1995-07-10"),
            "basicSalary": 5724693,
            "status": "Active",
            "group": "Admin",
            "description": "Patrocinor alo agnosco articulus."
        },
        {
            "username": "Patricia29",
            "firstName": "Nicolette",
            "lastName": "Mueller-Hahn",
            "email": "Jeffrey_Wisozk-Olson@gmail.com",
            "birthDate": new Date("2000-11-02"),
            "basicSalary": 6860714,
            "status": "Inactive",
            "group": "Admin",
            "description": "Cognomen cattus corrumpo adficio verbera deleo cribro."
        },
        {
            "username": "Jackson_Becker",
            "firstName": "Camryn",
            "lastName": "Hyatt",
            "email": "Don28@yahoo.com",
            "birthDate": new Date("1999-09-15"),
            "basicSalary": 13838151,
            "status": "Inactive",
            "group": "Admin",
            "description": "Vomica pax utroque subiungo adaugeo corrigo templum valeo curia canonicus."
        },
        {
            "username": "Deshaun.Cronin27",
            "firstName": "Judson",
            "lastName": "Strosin",
            "email": "Marguerite29@yahoo.com",
            "birthDate": new Date("2000-07-01"),
            "basicSalary": 13173375,
            "status": "Active",
            "group": "Admin",
            "description": "Calcar tripudio auditor."
        },
        {
            "username": "Dangelo49",
            "firstName": "Mandy",
            "lastName": "Harber",
            "email": "Brionna16@gmail.com",
            "birthDate": new Date("2000-03-19"),
            "basicSalary": 19517072,
            "status": "Active",
            "group": "Admin",
            "description": "Saepe provident cognatus."
        }
    ];

    private filteredEmployees: Employee[] = [];

    constructor(private dummy: DummyService) {
        this.filteredEmployees = this.employees;
    }

    getEmployees(): Observable<Employee[]> {
        return of(this.filteredEmployees);
    }

    searchEmployees(searchParams: any): Observable<Employee[]> {
        console.log(searchParams);
        const filteredEmployees = this.employees.filter(employee =>
            (!searchParams.username || employee.username.toLowerCase().includes(searchParams.username.toLowerCase())) &&
            (!searchParams.firstName || employee.firstName.toLowerCase().includes(searchParams.firstName.toLowerCase())) &&
            (!searchParams.lastName || employee.lastName.toLowerCase().includes(searchParams.lastName.toLowerCase()))
        );
        this.filteredEmployees = filteredEmployees;
        return of(filteredEmployees);

    }

    getEmployee(username: string): Observable<Employee | undefined> {
        const employee = this.employees.find(emp => emp.username === username);
        return of(employee);
    }

    addEmployee(employee: Employee): void {
        this.employees.push(employee);
    }

    updateEmployee(updatedEmployee: Employee): void {
        const index = this.employees.findIndex(emp => emp.username === updatedEmployee.username);
        if (index !== -1) {
            this.employees[index] = updatedEmployee;
        }
    }

    deleteEmployee(username: string): void {
        this.employees = this.employees.filter(emp => emp.username !== username);
    }
}
