import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GroupService {

    constructor() { }

    group: string[] = [
        'Admin',
        'Guest',
        'User',
        'Finance',
        'Accounting',
        'HR',
        'Manager',
        'IT Developer',
        'IT Infra',
        'Database Admin'
    ];
}
