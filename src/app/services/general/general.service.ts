import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GeneralService {

    constructor() { }

    formatCurrency(value: number): string {
        return 'Rp ' + value.toLocaleString('id', { minimumFractionDigits: 2 });
    }
}
