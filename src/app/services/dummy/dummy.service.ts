import { Injectable } from '@angular/core';
import { Employee } from 'src/app/model/employee/employee-model';
import { faker } from '@faker-js/faker';

@Injectable({
    providedIn: 'root'
})
export class DummyService {

    constructor() { }

    generateFakeUsers(numUsers: number = 100): Employee[] {
        let users: Employee[] = [];
        for (let i = 0; i < numUsers; i++) {
            let user: Employee = {
                username: faker.internet.userName(),
                lastName: faker.person.lastName(),
                firstName: faker.person.firstName(),
                email: faker.internet.email(),
                birthDate: faker.date.past({years: 10, refDate: '2000-12-31'}),
                basicSalary: faker.number.float({ min: 5000000, max: 20000000 }),  // Salary in Rupiah
                status: faker.helpers.arrayElement(['Active', 'Inactive']),
                group: faker.helpers.arrayElement(['Admin', 'User', 'Guest']),
                description: faker.lorem.sentence()
            };
            users.push(user);
        }
        // console.log(users);
        return users;
    }

    employees: Employee[] = [
        {
            "username": "Gust_Pagac",
            "firstName": "Brent",
            "lastName": "Legros",
            "email": "Dewitt.Smitham3@gmail.com",
            "birthDate": new Date("1991-06-16T08:05:38.377Z"),
            "basicSalary": 10098968.98525767,
            "status": "Inactive",
            "group": "Admin",
            "description": "Vox clementia eligendi non ustulo approbo."
        },
        {
            "username": "Lelia8",
            "firstName": "Deon",
            "lastName": "O'Kon",
            "email": "Madonna6@hotmail.com",
            "birthDate": new Date("1991-06-13T22:24:13.403Z"),
            "basicSalary": 9778747.28385359,
            "status": "Active",
            "group": "Guest",
            "description": "Excepturi quia ait."
        },
        {
            "username": "Andy25",
            "firstName": "Lindsay",
            "lastName": "Mann",
            "email": "Kali_Effertz31@yahoo.com",
            "birthDate": new Date("1995-02-02T21:28:35.964Z"),
            "basicSalary": 5906178.287696093,
            "status": "Inactive",
            "group": "Admin",
            "description": "Tui valde cum."
        },
        {
            "username": "Dustin_Weimann2",
            "firstName": "Name",
            "lastName": "Weber",
            "email": "Lorine.Dach@gmail.com",
            "birthDate": new Date("1995-10-14T21:45:34.918Z"),
            "basicSalary": 15779781.04003705,
            "status": "Inactive",
            "group": "Guest",
            "description": "Tamquam cupio viriliter utrum dens."
        },
        // {
        //     "username": "Antonette_Schiller93",
        //     "firstName": "Oswaldo",
        //     "lastName": "Kihn",
        //     "email": "Rosalyn18@yahoo.com",
        //     "birthDate": new Date("1994-01-11T15:14:29.883Z"),
        //     "basicSalary": 18973933.985689655,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Sapiente summisse creta distinctio iure angulus consectetur timidus vilitas vestrum."
        // },
        // {
        //     "username": "Veronica91",
        //     "firstName": "Mohammad",
        //     "lastName": "Cartwright",
        //     "email": "Jerald_Kunde@hotmail.com",
        //     "birthDate": new Date("1999-10-01T11:44:19.635Z"),
        //     "basicSalary": 16819595.257984474,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Capio coma quibusdam caelestis angulus adulatio cuppedia."
        // },
        // {
        //     "username": "Cheyanne92",
        //     "firstName": "Eunice",
        //     "lastName": "Keebler",
        //     "email": "Tyreek_Towne@yahoo.com",
        //     "birthDate": new Date("1995-01-07T03:24:48.875Z"),
        //     "basicSalary": 6046245.840843767,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Suggero vilis curia."
        // },
        // {
        //     "username": "Dawn.Gusikowski39",
        //     "firstName": "Danielle",
        //     "lastName": "Schuppe",
        //     "email": "Bella.Grady23@yahoo.com",
        //     "birthDate": new Date("1995-08-12T09:56:27.535Z"),
        //     "basicSalary": 9235941.419610754,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Consectetur adhuc defetiscor vociferor."
        // },
        // {
        //     "username": "Nels17",
        //     "firstName": "Marcus",
        //     "lastName": "Torphy",
        //     "email": "Pearline_Okuneva10@hotmail.com",
        //     "birthDate": new Date("1996-01-13T00:27:41.992Z"),
        //     "basicSalary": 8159314.623335376,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Aperiam aperte supra argumentum carpo caste temporibus aufero charisma adfero."
        // },
        // {
        //     "username": "Alec.Leannon64",
        //     "firstName": "Lucie",
        //     "lastName": "Tremblay",
        //     "email": "Brannon32@gmail.com",
        //     "birthDate": new Date("1991-06-30T22:39:30.463Z"),
        //     "basicSalary": 19733067.903434858,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Cui delinquo temeritas aggredior collum verumtamen debilito volo."
        // },
        // {
        //     "username": "Bertrand_Boyle",
        //     "firstName": "Carlotta",
        //     "lastName": "Okuneva",
        //     "email": "Lenny35@gmail.com",
        //     "birthDate": new Date("1999-05-04T03:18:05.784Z"),
        //     "basicSalary": 6134370.317449793,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Degenero articulus utroque speculum."
        // },
        // {
        //     "username": "Joe.Connelly",
        //     "firstName": "Baron",
        //     "lastName": "Kassulke",
        //     "email": "Fern.OKeefe85@hotmail.com",
        //     "birthDate": new Date("1994-08-29T18:31:22.303Z"),
        //     "basicSalary": 15611941.101960838,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Incidunt patruus ambitus colligo soluta."
        // },
        // {
        //     "username": "Edwardo60",
        //     "firstName": "Adrienne",
        //     "lastName": "Rice",
        //     "email": "Jarrod_Hane48@hotmail.com",
        //     "birthDate": new Date("1993-04-24T16:50:27.952Z"),
        //     "basicSalary": 15873943.836195394,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Vere aestivus verbum acies vulariter."
        // },
        // {
        //     "username": "Nola34",
        //     "firstName": "Samir",
        //     "lastName": "Lesch",
        //     "email": "Cordia_Hane93@yahoo.com",
        //     "birthDate": new Date("1992-08-24T05:49:01.669Z"),
        //     "basicSalary": 14786759.879207239,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Consectetur autem vulnero deficio vito sophismata."
        // },
        // {
        //     "username": "Francesco_Friesen0",
        //     "firstName": "Keyon",
        //     "lastName": "Lesch",
        //     "email": "Lonnie_Kuhn@hotmail.com",
        //     "birthDate": new Date("1997-05-02T13:10:23.541Z"),
        //     "basicSalary": 18129661.92374006,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Causa spectaculum mollitia angulus velum tum antepono capio concido tepesco."
        // },
        // {
        //     "username": "Isidro85",
        //     "firstName": "Marisol",
        //     "lastName": "MacGyver",
        //     "email": "Johnny_Hudson96@yahoo.com",
        //     "birthDate": new Date("1996-06-28T21:56:37.796Z"),
        //     "basicSalary": 9332304.968265817,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Bonus blanditiis veritatis coruscus dapifer verto vicinus aliquid."
        // },
        // {
        //     "username": "Jovany_Sauer72",
        //     "firstName": "Jamil",
        //     "lastName": "Wolf",
        //     "email": "Briana_Leffler@yahoo.com",
        //     "birthDate": new Date("2000-12-12T02:23:36.734Z"),
        //     "basicSalary": 18781028.863741085,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Teneo adversus ante aperio cribro verto terreo."
        // },
        // {
        //     "username": "Jaquan48",
        //     "firstName": "Eduardo",
        //     "lastName": "Von",
        //     "email": "Edwin.Runte@hotmail.com",
        //     "birthDate": new Date("1993-01-27T01:21:35.405Z"),
        //     "basicSalary": 8111466.120462865,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Strues minima clam modi bellicus terminatio admoveo admiratio uter dolorem."
        // },
        // {
        //     "username": "Julianne5",
        //     "firstName": "Jerod",
        //     "lastName": "Wyman",
        //     "email": "Esta.Klocko@yahoo.com",
        //     "birthDate": new Date("1996-10-11T21:18:07.675Z"),
        //     "basicSalary": 5974478.498101234,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Degenero apud subiungo adversus defendo."
        // },
        // {
        //     "username": "Junior66",
        //     "firstName": "Jeramy",
        //     "lastName": "Lubowitz-Halvorson",
        //     "email": "Dorian.Thompson31@gmail.com",
        //     "birthDate": new Date("1998-11-21T05:29:58.634Z"),
        //     "basicSalary": 12242078.313138336,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Adflicto viscus tenus absque adfectus."
        // },
        // {
        //     "username": "Dax0",
        //     "firstName": "Myrtis",
        //     "lastName": "Macejkovic",
        //     "email": "Raphaelle_Bayer12@gmail.com",
        //     "birthDate": new Date("1995-12-08T23:52:10.160Z"),
        //     "basicSalary": 15629557.298962027,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Auctor textor tergum vitae."
        // },
        // {
        //     "username": "Destiney82",
        //     "firstName": "Joaquin",
        //     "lastName": "Gerlach",
        //     "email": "Clifton_Rodriguez@gmail.com",
        //     "birthDate": new Date("1991-01-16T04:50:10.806Z"),
        //     "basicSalary": 12626711.318735033,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Trucido damno considero spes tenetur."
        // },
        // {
        //     "username": "Keagan.Cole",
        //     "firstName": "Federico",
        //     "lastName": "Rohan",
        //     "email": "Laurine85@gmail.com",
        //     "birthDate": new Date("2000-09-08T20:57:07.480Z"),
        //     "basicSalary": 7983669.338282198,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Catena tutis vel."
        // },
        // {
        //     "username": "Pietro_Denesik",
        //     "firstName": "Nedra",
        //     "lastName": "Sanford",
        //     "email": "Jorge90@hotmail.com",
        //     "birthDate": new Date("1994-03-07T21:11:57.501Z"),
        //     "basicSalary": 17807989.713037387,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Suspendo stips urbanus confugo aliquid."
        // },
        // {
        //     "username": "Sydnee10",
        //     "firstName": "Brock",
        //     "lastName": "McCullough",
        //     "email": "Patrick_Hand60@hotmail.com",
        //     "birthDate": new Date("1994-08-04T22:41:26.087Z"),
        //     "basicSalary": 12452878.557378426,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Ait aeternus crebro uter unde."
        // },
        // {
        //     "username": "Golden76",
        //     "firstName": "Lorena",
        //     "lastName": "Kshlerin",
        //     "email": "Elody_Miller58@yahoo.com",
        //     "birthDate": new Date("1998-04-18T02:29:00.756Z"),
        //     "basicSalary": 14537921.06709443,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Stips suasoria tempora curia."
        // },
        // {
        //     "username": "Keely.Jast72",
        //     "firstName": "Kadin",
        //     "lastName": "Boyle",
        //     "email": "Karianne_Heathcote96@yahoo.com",
        //     "birthDate": new Date("1994-04-19T10:53:10.203Z"),
        //     "basicSalary": 6093411.3373514265,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Demoror suppono truculenter cinis suspendo conventus."
        // },
        // {
        //     "username": "Irving.Willms21",
        //     "firstName": "Joanie",
        //     "lastName": "Jast",
        //     "email": "Samson23@yahoo.com",
        //     "birthDate": new Date("1995-12-03T13:05:53.571Z"),
        //     "basicSalary": 12399161.227513105,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Demoror vomer venio termes modi studio quibusdam comparo."
        // },
        // {
        //     "username": "Molly.Kassulke50",
        //     "firstName": "Olin",
        //     "lastName": "Abbott",
        //     "email": "Kiera.Schowalter-Klein@hotmail.com",
        //     "birthDate": new Date("2000-10-22T09:10:07.133Z"),
        //     "basicSalary": 12711200.616322458,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Tempus bestia conservo vespillo atqui aranea."
        // },
        // {
        //     "username": "Cassandra57",
        //     "firstName": "Alicia",
        //     "lastName": "Lynch",
        //     "email": "Major_Dietrich49@yahoo.com",
        //     "birthDate": new Date("1991-10-18T21:35:39.417Z"),
        //     "basicSalary": 17660687.554161996,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Compono decor ocer vicinus."
        // },
        // {
        //     "username": "Rod_Fritsch70",
        //     "firstName": "Hadley",
        //     "lastName": "Stark",
        //     "email": "Rozella_Ruecker51@gmail.com",
        //     "birthDate": new Date("1998-10-15T10:35:05.717Z"),
        //     "basicSalary": 11129219.598369673,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Tredecim spiculum repellat vallum vel sodalitas eum cena."
        // },
        // {
        //     "username": "Earnest42",
        //     "firstName": "Megane",
        //     "lastName": "Legros",
        //     "email": "Oma_Gutmann@yahoo.com",
        //     "birthDate": new Date("1999-01-15T00:29:16.197Z"),
        //     "basicSalary": 19304752.406897023,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Commodi defaeco admiratio delibero utroque depraedor."
        // },
        // {
        //     "username": "Jerel.Dicki49",
        //     "firstName": "Trinity",
        //     "lastName": "Kuhic",
        //     "email": "Linwood.Cassin-Hirthe64@gmail.com",
        //     "birthDate": new Date("2000-05-13T23:47:54.808Z"),
        //     "basicSalary": 17772021.000273526,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Audax speciosus cupiditate aetas."
        // },
        // {
        //     "username": "Manley99",
        //     "firstName": "Cristal",
        //     "lastName": "Baumbach",
        //     "email": "Avery_Rutherford@gmail.com",
        //     "birthDate": new Date("1997-03-12T03:01:38.114Z"),
        //     "basicSalary": 12581513.264449313,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Crustulum curto vociferor ratione."
        // },
        // {
        //     "username": "Rowena_Ferry7",
        //     "firstName": "Janice",
        //     "lastName": "Stracke",
        //     "email": "Merl.Shields@yahoo.com",
        //     "birthDate": new Date("1999-11-28T13:33:22.461Z"),
        //     "basicSalary": 5546216.544462368,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Carpo totus tersus ustilo nostrum veritas templum."
        // },
        // {
        //     "username": "Melyssa.Carroll41",
        //     "firstName": "Kirk",
        //     "lastName": "Ward",
        //     "email": "Era67@yahoo.com",
        //     "birthDate": new Date("1995-12-11T02:39:12.902Z"),
        //     "basicSalary": 9001841.160934418,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Spoliatio sulum apto annus spiritus celer."
        // },
        // {
        //     "username": "Natalia.Macejkovic63",
        //     "firstName": "Melany",
        //     "lastName": "Watsica",
        //     "email": "Ella_Jakubowski@gmail.com",
        //     "birthDate": new Date("1991-03-19T14:15:40.415Z"),
        //     "basicSalary": 18958790.03452137,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Vicinus bis contigo tondeo iusto vinum ademptio aegrotatio perferendis sto."
        // },
        // {
        //     "username": "Abbey41",
        //     "firstName": "Javon",
        //     "lastName": "Funk",
        //     "email": "Lennie.Mayert@yahoo.com",
        //     "birthDate": new Date("2000-10-07T18:37:48.078Z"),
        //     "basicSalary": 14407140.068942681,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Tantum tamdiu totam vobis curtus torqueo fugit delibero vicissitudo caecus."
        // },
        // {
        //     "username": "Sharon_MacGyver",
        //     "firstName": "Marianne",
        //     "lastName": "Mayer",
        //     "email": "Johnpaul_OKeefe@yahoo.com",
        //     "birthDate": new Date("1996-10-14T20:30:57.335Z"),
        //     "basicSalary": 11333776.355022565,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Maxime vulgivagus vapulus tergiversatio chirographum color peior ab."
        // },
        // {
        //     "username": "Juana.Mills",
        //     "firstName": "Avis",
        //     "lastName": "Kilback",
        //     "email": "Sadie_Wilderman@gmail.com",
        //     "birthDate": new Date("2000-08-07T12:53:02.193Z"),
        //     "basicSalary": 8650583.297712728,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Valetudo thesaurus contego benigne."
        // },
        // {
        //     "username": "Madelyn.Huel99",
        //     "firstName": "Curt",
        //     "lastName": "Aufderhar",
        //     "email": "Chauncey_Krajcik@gmail.com",
        //     "birthDate": new Date("1996-12-01T02:04:31.980Z"),
        //     "basicSalary": 14148866.391042247,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Pax aurum demonstro stips tantillus asporto."
        // },
        // {
        //     "username": "Aniya.Ortiz",
        //     "firstName": "Garfield",
        //     "lastName": "Mitchell",
        //     "email": "Kari_Yost@yahoo.com",
        //     "birthDate": new Date("1998-05-04T05:09:13.989Z"),
        //     "basicSalary": 12353211.047593504,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Officiis magnam rem vespillo tracto sumptus."
        // },
        // {
        //     "username": "Lew_Price",
        //     "firstName": "Freeman",
        //     "lastName": "Grant",
        //     "email": "Olaf_Blanda57@gmail.com",
        //     "birthDate": new Date("2000-05-26T20:36:49.800Z"),
        //     "basicSalary": 12648892.92884618,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Auctus custodia crapula tamdiu undique."
        // },
        // {
        //     "username": "Kassandra_Schimmel74",
        //     "firstName": "Mallory",
        //     "lastName": "O'Conner",
        //     "email": "Jessika.Reilly30@gmail.com",
        //     "birthDate": new Date("1994-12-09T19:50:04.736Z"),
        //     "basicSalary": 17900921.993423253,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Aranea unde magni damnatio."
        // },
        // {
        //     "username": "Martin.Keeling22",
        //     "firstName": "Alexandrea",
        //     "lastName": "Terry",
        //     "email": "Anita.Sipes@hotmail.com",
        //     "birthDate": new Date("1997-04-12T00:55:26.072Z"),
        //     "basicSalary": 11392223.166767508,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Degusto denuo circumvenio umbra deleo thymbra ultra dignissimos."
        // },
        // {
        //     "username": "Art.Lehner",
        //     "firstName": "Tamara",
        //     "lastName": "Lehner",
        //     "email": "Nolan_Lehner23@yahoo.com",
        //     "birthDate": new Date("1991-09-30T05:04:28.068Z"),
        //     "basicSalary": 12568300.514249131,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Vulpes arbor deorsum certus."
        // },
        // {
        //     "username": "Enoch.Ebert32",
        //     "firstName": "Antonette",
        //     "lastName": "Towne",
        //     "email": "Hermina_Muller@gmail.com",
        //     "birthDate": new Date("1996-06-22T20:03:29.995Z"),
        //     "basicSalary": 6699256.196152419,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Denego sol et molestiae claudeo tubineus cohors."
        // },
        // {
        //     "username": "Chaim.Morissette-Ward",
        //     "firstName": "Jarret",
        //     "lastName": "Yost",
        //     "email": "Tatyana.Wehner60@hotmail.com",
        //     "birthDate": new Date("2000-12-04T21:55:42.400Z"),
        //     "basicSalary": 9224094.933597371,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Ocer valetudo textus maxime."
        // },
        // {
        //     "username": "Damian50",
        //     "firstName": "Miller",
        //     "lastName": "Jacobi",
        //     "email": "Vicenta49@gmail.com",
        //     "birthDate": new Date("1997-08-16T09:04:34.605Z"),
        //     "basicSalary": 17048516.335198656,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Solus suggero carbo confero antepono sub beatus."
        // },
        // {
        //     "username": "Krystina_Hoppe36",
        //     "firstName": "Jayne",
        //     "lastName": "Reinger-Gerlach",
        //     "email": "Pietro_Kessler63@gmail.com",
        //     "birthDate": new Date("1998-05-20T06:02:41.290Z"),
        //     "basicSalary": 5865012.096473947,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Atrocitas surculus utrum nemo temperantia arcesso trucido."
        // },
        // {
        //     "username": "Caleb76",
        //     "firstName": "Kirk",
        //     "lastName": "Aufderhar",
        //     "email": "Suzanne.Morissette-Casper70@yahoo.com",
        //     "birthDate": new Date("1998-09-07T14:54:09.313Z"),
        //     "basicSalary": 14350146.773504093,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Cerno territo adimpleo cur desipio."
        // },
        // {
        //     "username": "Lue7",
        //     "firstName": "Kasandra",
        //     "lastName": "Johnston",
        //     "email": "Mortimer52@hotmail.com",
        //     "birthDate": new Date("1998-12-18T19:13:52.181Z"),
        //     "basicSalary": 5438101.022737101,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Aestivus aureus decipio auxilium."
        // },
        // {
        //     "username": "Kaelyn.Labadie",
        //     "firstName": "Julie",
        //     "lastName": "Homenick",
        //     "email": "Marilyne38@gmail.com",
        //     "birthDate": new Date("1997-01-23T19:06:11.327Z"),
        //     "basicSalary": 12036540.220724419,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Asporto timor voluptates timidus bibo defetiscor crux quas sustineo vomer."
        // },
        // {
        //     "username": "Jean_Feil",
        //     "firstName": "Joseph",
        //     "lastName": "Rath-D'Amore",
        //     "email": "Casper_Rice@yahoo.com",
        //     "birthDate": new Date("1996-09-11T21:19:51.952Z"),
        //     "basicSalary": 9300938.611850142,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Timor tonsor tres repellat custodia voluptatem solus vorago."
        // },
        // {
        //     "username": "Willow.Zboncak84",
        //     "firstName": "Cassandra",
        //     "lastName": "Mitchell",
        //     "email": "Jaunita_Williamson87@yahoo.com",
        //     "birthDate": new Date("1999-02-15T18:05:20.650Z"),
        //     "basicSalary": 17486374.129075557,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Maiores cupiditate peior porro dolores apto volaticus condico dapifer."
        // },
        // {
        //     "username": "Joelle46",
        //     "firstName": "Gerald",
        //     "lastName": "Satterfield",
        //     "email": "Jalen94@hotmail.com",
        //     "birthDate": new Date("2000-11-17T17:35:30.921Z"),
        //     "basicSalary": 16827315.39942324,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "A tergiversatio voluntarius."
        // },
        // {
        //     "username": "Ottilie64",
        //     "firstName": "Maya",
        //     "lastName": "Kutch",
        //     "email": "Brayan24@gmail.com",
        //     "birthDate": new Date("1997-08-13T07:12:57.182Z"),
        //     "basicSalary": 9535822.980105877,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Pecto stultus validus degenero derelinquo."
        // },
        // {
        //     "username": "Allie49",
        //     "firstName": "Hillary",
        //     "lastName": "Gutkowski",
        //     "email": "Serena_Dickinson53@hotmail.com",
        //     "birthDate": new Date("1999-11-28T03:01:12.992Z"),
        //     "basicSalary": 6011634.668102488,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Trepide amor testimonium alo tabgo auditor apparatus vitae."
        // },
        // {
        //     "username": "Sierra_Buckridge",
        //     "firstName": "Brett",
        //     "lastName": "Rodriguez",
        //     "email": "Wilhelm_Welch12@yahoo.com",
        //     "birthDate": new Date("1991-12-04T13:18:38.065Z"),
        //     "basicSalary": 13959053.356666118,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Versus laborum cattus suspendo sunt cotidie adficio sui spargo crudelis."
        // },
        // {
        //     "username": "Hannah88",
        //     "firstName": "Maximilian",
        //     "lastName": "Renner",
        //     "email": "Emmet_Runolfsdottir@hotmail.com",
        //     "birthDate": new Date("1997-07-26T13:31:39.701Z"),
        //     "basicSalary": 19107492.495095357,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Vapulus neque sit verus auxilium."
        // },
        // {
        //     "username": "Marisa.Lubowitz51",
        //     "firstName": "Joseph",
        //     "lastName": "McGlynn",
        //     "email": "Sean.Hartmann@gmail.com",
        //     "birthDate": new Date("1995-07-25T12:11:15.696Z"),
        //     "basicSalary": 12196297.381306067,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Tandem conspergo bestia."
        // },
        // {
        //     "username": "Rudolph3",
        //     "firstName": "Marcelino",
        //     "lastName": "Schowalter",
        //     "email": "Aditya.Corwin@hotmail.com",
        //     "birthDate": new Date("1998-10-20T03:02:47.232Z"),
        //     "basicSalary": 13394863.602006808,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Textus sapiente adflicto amaritudo conicio."
        // },
        // {
        //     "username": "Adell_Gulgowski-Hudson",
        //     "firstName": "Bonita",
        //     "lastName": "Schultz",
        //     "email": "Fermin_Kilback96@yahoo.com",
        //     "birthDate": new Date("1991-05-25T20:14:28.840Z"),
        //     "basicSalary": 10669776.538852602,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Abbas sumo eum vulgus theologus veritas cresco carpo."
        // },
        // {
        //     "username": "Trace_Terry",
        //     "firstName": "Lennie",
        //     "lastName": "Von",
        //     "email": "Kathryne_Beier@gmail.com",
        //     "birthDate": new Date("2000-01-01T06:15:56.037Z"),
        //     "basicSalary": 6093883.0429222435,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Quidem adiuvo spoliatio addo amo addo catena ustilo absque adnuo."
        // },
        // {
        //     "username": "Heath_Hackett83",
        //     "firstName": "Zackery",
        //     "lastName": "Stiedemann",
        //     "email": "Aurore3@yahoo.com",
        //     "birthDate": new Date("1994-03-28T08:05:01.484Z"),
        //     "basicSalary": 19738571.47502713,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Combibo casso coniuratio absum magni."
        // },
        // {
        //     "username": "Camylle_Fay49",
        //     "firstName": "Americo",
        //     "lastName": "Schuster",
        //     "email": "Fausto_Bergstrom@hotmail.com",
        //     "birthDate": new Date("1996-01-22T07:46:15.159Z"),
        //     "basicSalary": 6420757.932355627,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Quasi curatio territo tenuis derelinquo cibo nesciunt arcesso."
        // },
        // {
        //     "username": "Prudence.Bogan43",
        //     "firstName": "Annabell",
        //     "lastName": "Schiller",
        //     "email": "Roger.Shields95@yahoo.com",
        //     "birthDate": new Date("1997-07-08T12:59:56.931Z"),
        //     "basicSalary": 6373256.869846955,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Conventus nesciunt bene absconditus."
        // },
        // {
        //     "username": "Marcelina83",
        //     "firstName": "Junius",
        //     "lastName": "Sauer",
        //     "email": "Raina.Littel@yahoo.com",
        //     "birthDate": new Date("1993-08-25T08:41:00.714Z"),
        //     "basicSalary": 9656074.893428013,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Temptatio cruciamentum denuncio acceptus sollers allatus."
        // },
        // {
        //     "username": "Michelle70",
        //     "firstName": "Dedrick",
        //     "lastName": "Wisozk",
        //     "email": "Jasen73@yahoo.com",
        //     "birthDate": new Date("1993-09-02T07:17:06.738Z"),
        //     "basicSalary": 5603336.357744411,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Carbo curtus color cedo vinitor."
        // },
        // {
        //     "username": "Kallie.Reichel94",
        //     "firstName": "Nedra",
        //     "lastName": "Russel",
        //     "email": "Dorian.McClure12@gmail.com",
        //     "birthDate": new Date("1999-10-08T07:52:29.428Z"),
        //     "basicSalary": 10422250.430565327,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Talus stips odit utpote tripudio crur."
        // },
        // {
        //     "username": "Serenity_Littel83",
        //     "firstName": "Braxton",
        //     "lastName": "Boehm",
        //     "email": "Aurelie26@hotmail.com",
        //     "birthDate": new Date("1994-03-31T10:15:44.564Z"),
        //     "basicSalary": 14703357.627149671,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Defessus vinitor supra stillicidium textus."
        // },
        // {
        //     "username": "Donny76",
        //     "firstName": "Coralie",
        //     "lastName": "Kuhn",
        //     "email": "Jenifer.Larson@yahoo.com",
        //     "birthDate": new Date("1999-11-03T03:27:27.036Z"),
        //     "basicSalary": 10237855.479354039,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Laborum bonus dapifer."
        // },
        // {
        //     "username": "Vincenza_Bergstrom",
        //     "firstName": "Assunta",
        //     "lastName": "Schumm",
        //     "email": "Everette.Cruickshank@hotmail.com",
        //     "birthDate": new Date("1997-03-27T06:51:41.619Z"),
        //     "basicSalary": 19180910.266004503,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Varius adfero tersus absque defessus tribuo via tabesco."
        // },
        // {
        //     "username": "Lydia10",
        //     "firstName": "Jaron",
        //     "lastName": "Heller",
        //     "email": "Caesar_Wiza64@gmail.com",
        //     "birthDate": new Date("1993-04-24T12:29:18.736Z"),
        //     "basicSalary": 10508300.86111091,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Damnatio vestrum cursim molestiae esse."
        // },
        // {
        //     "username": "America.Ruecker",
        //     "firstName": "Linwood",
        //     "lastName": "Gerhold",
        //     "email": "Jaylin_Mueller@yahoo.com",
        //     "birthDate": new Date("1991-06-21T18:07:32.118Z"),
        //     "basicSalary": 5790429.705521092,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Vulgo tristis angulus tempora."
        // },
        // {
        //     "username": "Summer43",
        //     "firstName": "Kathryne",
        //     "lastName": "Glover",
        //     "email": "Damian30@gmail.com",
        //     "birthDate": new Date("1999-09-01T01:47:39.370Z"),
        //     "basicSalary": 13187751.719960943,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Clibanus velociter vesica defero solum."
        // },
        // {
        //     "username": "Xavier.Fahey",
        //     "firstName": "Jasper",
        //     "lastName": "Pagac",
        //     "email": "Agustina_Mante@yahoo.com",
        //     "birthDate": new Date("1991-03-01T20:55:53.093Z"),
        //     "basicSalary": 17389098.13691862,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Paens quis alii vulnero sonitus benigne versus ago tempus dolorem."
        // },
        // {
        //     "username": "Eldred.Kuvalis61",
        //     "firstName": "Paolo",
        //     "lastName": "Blanda",
        //     "email": "Barry93@gmail.com",
        //     "birthDate": new Date("1991-02-14T00:14:33.422Z"),
        //     "basicSalary": 18433530.046604574,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Averto rerum tenus bardus ago dolores vesica deprecator est."
        // },
        // {
        //     "username": "Brittany_Pollich78",
        //     "firstName": "Anastasia",
        //     "lastName": "Baumbach",
        //     "email": "Ava_Lang89@hotmail.com",
        //     "birthDate": new Date("1991-12-01T12:43:33.773Z"),
        //     "basicSalary": 16174191.574100405,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Curso sufficio subnecto terror carbo aufero sollicito."
        // },
        // {
        //     "username": "Trinity.Watsica",
        //     "firstName": "Antonia",
        //     "lastName": "Becker",
        //     "email": "Dorian83@yahoo.com",
        //     "birthDate": new Date("1994-10-23T16:20:44.979Z"),
        //     "basicSalary": 5549906.670348719,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Conduco cicuta apto crapula."
        // },
        // {
        //     "username": "Emmy.Donnelly",
        //     "firstName": "Jailyn",
        //     "lastName": "Johnson",
        //     "email": "Ephraim.Waters@yahoo.com",
        //     "birthDate": new Date("1997-03-04T08:17:25.041Z"),
        //     "basicSalary": 18653185.30006334,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Callide adeo deorsum vomer cohors deprimo vitium desipio adiuvo basium."
        // },
        // {
        //     "username": "Jaquelin_Barrows",
        //     "firstName": "Trenton",
        //     "lastName": "Dietrich",
        //     "email": "Mustafa62@hotmail.com",
        //     "birthDate": new Date("1999-10-23T15:04:27.402Z"),
        //     "basicSalary": 19034972.094232216,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Iste urbanus clam tergo surculus aestas."
        // },
        // {
        //     "username": "Ray.Schuppe22",
        //     "firstName": "Kenya",
        //     "lastName": "Beahan",
        //     "email": "Vivien1@gmail.com",
        //     "birthDate": new Date("1999-02-27T10:31:02.951Z"),
        //     "basicSalary": 12054532.283218578,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Cena verbera adeo corona."
        // },
        // {
        //     "username": "Lesley93",
        //     "firstName": "Grayson",
        //     "lastName": "Nitzsche",
        //     "email": "Priscilla.Jacobs@yahoo.com",
        //     "birthDate": new Date("1995-01-26T00:00:25.347Z"),
        //     "basicSalary": 11987223.84753637,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Vomica decens voro pectus coepi textilis sollicito defetiscor nobis ducimus."
        // },
        // {
        //     "username": "Jabari23",
        //     "firstName": "Ronaldo",
        //     "lastName": "Collier",
        //     "email": "Tracey.McLaughlin94@yahoo.com",
        //     "birthDate": new Date("1993-03-27T18:38:09.131Z"),
        //     "basicSalary": 17273190.57798013,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Vociferor vomer culpo studio suggero."
        // },
        // {
        //     "username": "Zoie_Ruecker4",
        //     "firstName": "Devon",
        //     "lastName": "Ritchie",
        //     "email": "Carolina30@hotmail.com",
        //     "birthDate": new Date("1995-01-01T16:43:46.180Z"),
        //     "basicSalary": 10655401.983531192,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Casus textus correptius caries contigo quos officia turpis."
        // },
        // {
        //     "username": "Ophelia87",
        //     "firstName": "Bernie",
        //     "lastName": "Pfeffer",
        //     "email": "Sandrine.Keeling@hotmail.com",
        //     "birthDate": new Date("1992-04-08T01:03:00.330Z"),
        //     "basicSalary": 7482137.012993917,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Complectus blanditiis statua uterque cognatus sustineo valetudo."
        // },
        // {
        //     "username": "Devyn72",
        //     "firstName": "Emory",
        //     "lastName": "Greenfelder",
        //     "email": "Ryder2@yahoo.com",
        //     "birthDate": new Date("1991-09-21T08:24:32.057Z"),
        //     "basicSalary": 8211624.886607751,
        //     "status": "Inactive",
        //     "group": "Guest",
        //     "description": "Solitudo tolero ipsam clam aegrus velut aggredior vesica."
        // },
        // {
        //     "username": "Wilma_Sipes",
        //     "firstName": "Deondre",
        //     "lastName": "Bayer",
        //     "email": "Kellie.Shields@yahoo.com",
        //     "birthDate": new Date("1992-11-18T12:08:06.895Z"),
        //     "basicSalary": 19323299.910174683,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Crustulum arceo vesica."
        // },
        // {
        //     "username": "Tatyana_Zieme23",
        //     "firstName": "Ima",
        //     "lastName": "Daugherty",
        //     "email": "Berry87@yahoo.com",
        //     "birthDate": new Date("1998-04-20T14:34:06.542Z"),
        //     "basicSalary": 11881363.805150613,
        //     "status": "Inactive",
        //     "group": "User",
        //     "description": "Saepe amplitudo admitto vacuus aegre vulnero undique quo compello."
        // },
        // {
        //     "username": "Estella.Weissnat",
        //     "firstName": "Kavon",
        //     "lastName": "Kihn",
        //     "email": "Abner.Shields35@hotmail.com",
        //     "birthDate": new Date("1991-05-10T16:03:47.455Z"),
        //     "basicSalary": 11076305.663445964,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Modi aiunt solitudo virgo explicabo repudiandae asper culpo accusamus demo."
        // },
        // {
        //     "username": "Carlo.Olson",
        //     "firstName": "Alysha",
        //     "lastName": "Homenick",
        //     "email": "Charity_Ratke27@yahoo.com",
        //     "birthDate": new Date("1993-04-15T03:52:08.570Z"),
        //     "basicSalary": 19364262.794842944,
        //     "status": "Active",
        //     "group": "Guest",
        //     "description": "Crustulum amitto virga eligendi abundans."
        // },
        // {
        //     "username": "Arnoldo.Fay",
        //     "firstName": "Patrick",
        //     "lastName": "Runolfsson",
        //     "email": "Vicente.Rath@yahoo.com",
        //     "birthDate": new Date("2000-01-19T18:34:47.495Z"),
        //     "basicSalary": 9130181.883228943,
        //     "status": "Active",
        //     "group": "User",
        //     "description": "Tum audax ambitus error decerno audacia."
        // },
        // {
        //     "username": "Blanche_DAmore68",
        //     "firstName": "Vincenzo",
        //     "lastName": "Hilpert",
        //     "email": "Ashton.King12@yahoo.com",
        //     "birthDate": new Date("1999-01-09T17:40:45.149Z"),
        //     "basicSalary": 16839281.20881319,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Supra tergiversatio tondeo carmen demergo tunc."
        // },
        // {
        //     "username": "Jacklyn52",
        //     "firstName": "Martine",
        //     "lastName": "Christiansen",
        //     "email": "Dee_Beatty54@yahoo.com",
        //     "birthDate": new Date("1993-08-24T16:28:09.813Z"),
        //     "basicSalary": 9517600.453691557,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Averto acerbitas subvenio."
        // },
        // {
        //     "username": "Aditya.Jaskolski",
        //     "firstName": "Blaze",
        //     "lastName": "Murphy",
        //     "email": "Charity_Sipes@gmail.com",
        //     "birthDate": new Date("1995-07-10T11:11:43.663Z"),
        //     "basicSalary": 5724693.184019998,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Patrocinor alo agnosco articulus."
        // },
        // {
        //     "username": "Patricia29",
        //     "firstName": "Nicolette",
        //     "lastName": "Mueller-Hahn",
        //     "email": "Jeffrey_Wisozk-Olson@gmail.com",
        //     "birthDate": new Date("2000-11-02T08:23:01.861Z"),
        //     "basicSalary": 6860714.645590633,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Cognomen cattus corrumpo adficio verbera deleo cribro."
        // },
        // {
        //     "username": "Jackson_Becker",
        //     "firstName": "Camryn",
        //     "lastName": "Hyatt",
        //     "email": "Don28@yahoo.com",
        //     "birthDate": new Date("1999-09-15T09:35:34.538Z"),
        //     "basicSalary": 13838151.046074927,
        //     "status": "Inactive",
        //     "group": "Admin",
        //     "description": "Vomica pax utroque subiungo adaugeo corrigo templum valeo curia canonicus."
        // },
        // {
        //     "username": "Deshaun.Cronin27",
        //     "firstName": "Judson",
        //     "lastName": "Strosin",
        //     "email": "Marguerite29@yahoo.com",
        //     "birthDate": new Date("2000-07-01T21:21:04.100Z"),
        //     "basicSalary": 13173375.97720325,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Calcar tripudio auditor."
        // },
        // {
        //     "username": "Dangelo49",
        //     "firstName": "Mandy",
        //     "lastName": "Harber",
        //     "email": "Brionna16@gmail.com",
        //     "birthDate": new Date("2000-03-19T09:32:48.649Z"),
        //     "basicSalary": 19517072.31462933,
        //     "status": "Active",
        //     "group": "Admin",
        //     "description": "Saepe provident cognatus."
        // }
    ];
}
