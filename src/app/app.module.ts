import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { LoginComponent } from './pages/login/login.component';
import { InMemoryDataService } from './services/memory/in-memory-data.service';
import { DummyService } from './services/dummy/dummy.service';
import { AuthGuard } from './auth/auth.guard';

// import { EmployeeModule } from './pages/employee/employee.module';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientInMemoryWebApiModule.forRoot(
            InMemoryDataService, {dataEncapsulation: false}
        )

    ],
    providers: [DummyService, AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule { }
